import { Component } from '@angular/core';
import { NaturalLanguageProvider } from '../../providers/natural-language/natural-language';
import { PeopleProvider } from '../../providers/people/people';
import { AppVersion } from '@ionic-native/app-version';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [ NaturalLanguageProvider, AppVersion ],
})

export class HomePage {
  messageInput: string = '';
  messages: Array<Object> = [];
  actions: Array<string> = [];
  isLoading: boolean = false;
  version: string;
  state: string;

  constructor(public naturalLanguageProvider: NaturalLanguageProvider, public people: PeopleProvider, public appVersion: AppVersion) {
    this.messages = [
      {from:'bot', text:'Hello I\'m BB-8!'},
      {from:'bot', text: this.naturalLanguageProvider.help()},
      {from:'bot', text:'If you need help, just ask me :)'},
    ];

    this.appVersion.getVersionNumber().then(version => this.version = version).catch(console.error);
  }

  sendMessage(msg) {
    this.messageInput = '';
    this.addMessage('user', msg);
    
    let nl_state = this.naturalLanguageProvider.state;
    
    // clear old actions if we are in a new state
    if (nl_state && nl_state !== this.state) {
      this.clearActions;
      this.state = nl_state;
    }
    
    let actions = this.naturalLanguageProvider.getActionsForMessage(msg);

    // The user is requesting some information about an item (like the name of an actor)
    // We are already in the state of an action
    if(this.state && this.naturalLanguageProvider.getService() && actions.length === 0) {
      this.isLoading = true;
      this.naturalLanguageProvider
      .getService().search(msg)
      .subscribe(function(result) {
        this.isLoading=false;

        if(result.count > 10) {
          this.addMessage('bot', 'I have too many result in my database. Can you be more precise ?');
        } else if(result.count > 1 && result.count <= 10) {
          this.addMessage('bot', 'I found multiple result for your request, which one do you prefer ?');
          this.actions = result.results.map(r => r.name || r.title);
        } else if(result.count === 0){
          this.addMessage('bot', "*Some sad beep*. I haven't found any result in my database.");
        } else {
          this.addMessage('bot', this.naturalLanguageProvider.getService().format(result.results[0]));
        }
      }.bind(this));
      // The user triggered an action (like people or films)
    } else {
      if (actions.length > 1) {
        actions.map(this.addAction.bind(this));
      }

      this.addMessage('bot', this.naturalLanguageProvider.replyForActions(actions));
    }
  }

  onClickAction(action) {
    this.clearActions();

    if(this.naturalLanguageProvider.state){
      this.isLoading = true;
      this.naturalLanguageProvider.getService().search(action).subscribe(
        function(result){
            this.isLoading = false;
            this.addMessage('bot', this.naturalLanguageProvider.getService().format(result.results[0]));
        }.bind(this)
      );

    } else {
      this.addMessage('bot', this.naturalLanguageProvider.callFnForAction(action));
    }
  }

  addMessage(from, msg) {
    this.messages.push({
      from: from,
      text: msg
    });
  }

  addAction(action) {
    this.actions.push(action);
  }

  clearActions() {
    this.actions = [];
  }
}