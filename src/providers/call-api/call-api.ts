import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import { LocalStorageProvider } from '../local-storage/local-storage';
import { isArray } from 'ionic-angular/util/util';

/*
  Generated class for the CallApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CallApiProvider {

  protected url: string = "https://swapi.co/api";

  constructor(protected http: HttpClient, protected localStorageProvider: LocalStorageProvider) {}

  protected request(url:string): Observable<any>{
    let obs: Observable<any>;
    let cache = this.localStorageProvider.get(url);
    if(cache){
      obs = Observable.of(cache);
    } else {
      obs = this.http
              .get(url)
              .do((reponse: any) => this.localStorageProvider.set(url, reponse))
    }
    return obs;
  }

  protected getAll(){
    return this.request(this.url);
  }

  protected get(id: number){
    return this.request(`${this.url}${id}`);
  }

  public search(name: string){
    let searchUrl = `${this.url}?search=${encodeURI(name)}`;
    return this.request(searchUrl);
  }

  protected format(object: any): string {
    let str: string = '';

    for(let key in object) {
      let ownALink =  object[key].includes('http') || (isArray(object[key]) && object[key].every(val => val.includes('http')));
      // If the value of the key has no http link, or not a created or edited field we print it
      
      if(!(ownALink || key === "created" || key === "edited")) {
        str += `${key}: ${object[key]},\n`;
      }
    }

    return str;
  }
}

