import { Injectable } from '@angular/core';
import { PeopleProvider } from '../people/people';
import { FilmsProvider } from '../films/films';
import { CallApiProvider } from '../call-api/call-api';
import { SpeciesProvider } from '../species/species';
import { VehiclesProvider } from '../vehicles/vehicles';
import { StarshipsProvider } from '../starships/starships';
import { PlanetsProvider } from '../planets/planets';

/*
  Generated class for the NaturalLanguageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NaturalLanguageProvider {
  keywords: Array<String>;
  public state: string;

  constructor(public peopleService: PeopleProvider, public filmsService: FilmsProvider, public speciesService: SpeciesProvider, public vehiclesService: VehiclesProvider, public starshipsService : StarshipsProvider, public planetsService: PlanetsProvider) {
    this.keywords = ["help", "people", "films", "species", "vehicles", "starships", "planets", "info"];
  }
  
  getService() : CallApiProvider {
    switch (this.state) {
      case 'people':
        return this.peopleService;
      case 'films':
        return this.filmsService;
      case 'species':
        return this.speciesService;
      case 'vehicles':
        return this.vehiclesService;
      case 'starships':
        return this.starshipsService;
      case 'planets':
        return this.planetsService;
      default:
        return undefined;
    }

  }

  getActionsForMessage(msg) {
    return msg.split(" ").filter(word => this.keywords.includes(word.toLowerCase()));
  };

  replyForActions(actions) {
    // A doubt exists, let the user choose an action
    if (actions.length > 1) {
      return 'Ok, please choose an action to do';
    } else {
      return this.callFnForAction(actions[0] || "");
    }
  }

  callFnForAction(action) {
    switch (action.toLowerCase()) {
      case 'help':
        return this.help();
      case 'people':
        return this.people();
      case 'films':
        return this.films();
      case 'species':
        return this.species();
      case 'vehicles':
        return this.vehicles();
      case 'starships':
        return this.starships();
      case 'planets':
        return this.planets();
      case 'info':
        return this.association();
      default:
        return "Beep blup bep. Error system, could you redefine your demand please ?";
    }
  }

  help() {
    return "Do you want to discover the Star Wars saga? Ask me something like 'people' or 'films'. If you want information about me, tap info. You can also type 'help' if you need it.";
  }

  people() {
    this.state = "people";
    return "Oh you want to know the characters of Star Wars ? Ok, you can ask me anything, just give me their names."
  }

  films() {
    this.state = "films";
    return "Type 'all' if you want the full list or type a title.";
  }
  
  association() {
    return "I am serving Star Wars Fan Association (SWFA). This association regroups fans from all over the world and make fan events. We also help sick child to grant their wishes. Their members love to spread Star Wars knowledge."
  }

  planets() {
    this.state = "planets";
    return "Awesome planets in an awesome galaxy. tap a name, and I will found something for you."
  }

  species() {
    this.state = "species";
    return "I know many species, it reminds me, What's the only thing shorter than an Ewok ? A clothes-lined Scout Trooper... OK, Which species do you want to get informations ?";
  }

  vehicles() {
    this.state = "vehicles";
    return "So many vehicles, can you give me something to search ?"
  }

  starships() {
    this.state = "starships";
    return "Yeah, my speciality. Paul and I love starships. Give me a name, and I will found anything !"
  }
}
