import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CallApiProvider } from '../call-api/call-api';
import { LocalStorageProvider } from '../local-storage/local-storage';

@Injectable()
export class FilmsProvider extends CallApiProvider {

  constructor(public http: HttpClient, public localStorageProvider: LocalStorageProvider) {
    super(http, localStorageProvider);
    this.url = `${this.url}/films/`;
  }

  public format(film){
    return `Title : ${film.title}, \n
            Director: ${film.director}, \n
            Producer: ${film.producer}, \n
            Release date: ${film.release_date} \n
            ${film.opening_crawl}`;
  }

  public search(term: string) {
    if (term === 'all') {
      return this.request(this.url);
    } else {
      return super.search(term);
    }
  }
}
