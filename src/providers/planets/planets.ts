import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CallApiProvider } from '../call-api/call-api';
import { LocalStorageProvider } from '../local-storage/local-storage';

/*
  Generated class for the PlanetsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PlanetsProvider extends CallApiProvider{

  constructor(public http: HttpClient, public localStorageProvider: LocalStorageProvider) {
    super(http, localStorageProvider);
    this.url = `${this.url}/planets/`;
  }
}
