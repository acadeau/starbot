import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CallApiProvider } from '../call-api/call-api';
import { LocalStorageProvider } from '../local-storage/local-storage';

/*
  Generated class for the PeopleProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PeopleProvider extends CallApiProvider{

  constructor(public http: HttpClient, public localStorageProvider: LocalStorageProvider) {
    super(http, localStorageProvider);
    this.url = `${this.url}/people/`;
  }

  public format(character){
    return `Name : ${character.name}, \n
            Height: ${character.height}, \n
            Mass: ${character.mass}, \n
            Hair color: ${character.hair_color}, \n
            Skin color: ${character.skin_color}, \n
            Eye color: ${character.eye_color}, \n
            Birth Year: ${character.birth_year}, \n
            Gender: ${character.gender}`;
  }
}
