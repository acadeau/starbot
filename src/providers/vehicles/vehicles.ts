import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CallApiProvider } from '../call-api/call-api';
import { LocalStorageProvider } from '../local-storage/local-storage';

/*
  Generated class for the VehiclesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class VehiclesProvider extends CallApiProvider {

  constructor(public http: HttpClient, public localStorageProvider: LocalStorageProvider ) {
    super(http, localStorageProvider);
    this.url = `${this.url}/vehicles/`;
  }

}
