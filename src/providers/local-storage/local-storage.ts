import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the LocalStorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocalStorageProvider {

  constructor() {
    console.log('Hello LocalStorageProvider Provider');
  }

  set(key: string, value: any){
    let oldValue = this.get(key);
    if(oldValue){
      value.results.concat(oldValue.results);
    } 
    localStorage.setItem(key, JSON.stringify(value));
  }

  get(key:string) {
    return JSON.parse(localStorage.getItem(key));
  }


}
