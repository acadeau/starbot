import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NaturalLanguageProvider } from '../providers/natural-language/natural-language';
import { HelpProvider } from '../providers/help/help';
import { PeopleProvider } from '../providers/people/people';
import { FilmsProvider } from '../providers/films/films';
import { CallApiProvider } from '../providers/call-api/call-api';
import { LocalStorageProvider } from '../providers/local-storage/local-storage';
import { HttpClientModule } from '@angular/common/http';
import { SpeciesProvider } from '../providers/species/species';
import { VehiclesProvider } from '../providers/vehicles/vehicles';
import { StarshipsProvider } from '../providers/starships/starships';
import { PlanetsProvider } from '../providers/planets/planets';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    NaturalLanguageProvider,
    HelpProvider,
    PeopleProvider,
    FilmsProvider,
    CallApiProvider,
    LocalStorageProvider,
    SpeciesProvider,
    VehiclesProvider,
    StarshipsProvider,
    PlanetsProvider
  ]
})
export class AppModule {}
