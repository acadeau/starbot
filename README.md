# A propos
Starbot est un projet visant à permettre aux utilisateurs de l'application de
découvrir la saga Star Wars. Il est possible de demander la liste des
personnages ou des films ainsi que des informations sur l'assocation.

L'application peut-être retrouvée sur le [Google Play store]().

# Pré-requis
- Node >= 9.2.0
- Npm >= 5.5.0
- Ionic >= 3.19.0

# Quickstart
- `git clone git@gitlab.com:acadeau/starbot.git`
- `cd starbot`
- `npm install`
- `ionic serve --lab`

